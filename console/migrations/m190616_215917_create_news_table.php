<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news}}`.
 */
class m190616_215917_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->text(),
        ]);

        $this->insert('news', [
            'title' => 'first',
            'description' => 'first description',
        ]);
        $this->insert('news', [
            'title' => 'second',
            'description' => 'second description',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('news');

        $this->dropTable('news');
    }
}
