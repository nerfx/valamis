<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news_comment}}`.
 */
class m190616_215944_create_news_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('news_comment', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer(),
            'text' => $this->text(),
        ]);

        $this->createIndex(
            'idx-news_comment-news_id',
            'news_comment',
            'news_id'
        );

        $this->addForeignKey(
            'fk-news_comment-news_id',
            'news_comment',
            'news_id',
            'news',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-news_comment-news_id',
            'news_comment'
        );

        $this->dropIndex(
            'idx-news_comment-news_id',
            'news_comment'
        );

        $this->dropTable('news_comment');
    }
}
