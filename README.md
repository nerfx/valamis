Docker installation
-
```
docker-compose up -d
docker-compose run --rm api composer install
docker-compose run --rm api php init --env=Development --overwrite=All
docker-compose run --rm api php yii migrate
```

Actions
-
```
List: GET http://localhost:22080/v1/news
List with comments: GET http://localhost:22080/v1/news?expand=comments
View: GET http://localhost:22080/v1/news/<id>
Create: POST http://localhost:22080/v1/news
Edit: PUT|PATCH http://localhost:22080/v1/news/<id>
Delete: DELETE http://localhost:22080/v1/news/<id>
Comments list: GET http://localhost:22080/v1/news/comments/<id>
Create comment: POST http://localhost:22080/v1/news/comments/<id>
```