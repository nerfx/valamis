<?php
/**
 * Created by PhpStorm.
 * User: Nerf
 */

namespace api\controllers;

use api\models\NewsCommentResource;
use api\models\NewsResource;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class NewsController extends ActiveController
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = NewsResource::class;

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['comments'] = ['GET'];
        $verbs['create-comment'] = ['POST'];

        return $verbs;
    }

    /**
     * @return ActiveDataProvider
     */
    public function prepareDataProvider()
    {
        return new ActiveDataProvider([
            'query' => NewsResource::find(),
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]],
        ]);
    }

    /**
     * @param int $id
     * @return ActiveDataProvider
     */
    public function actionComments($id)
    {
        $model = $this->findModel($id);

        return new ActiveDataProvider([
            'query' => $model->getCommentResources(),
            'sort' => ['defaultOrder' => ['id' => SORT_ASC]],
        ]);
    }

    /**
     * @param int $id
     * @return NewsCommentResource
     * @throws ServerErrorHttpException
     */
    public function actionCreateComment($id)
    {
        $news = $this->findModel($id);

        $model = new NewsCommentResource([
            'news_id' => $news->id,
        ]);
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }

    /**
     * @param int $id
     * @return NewsResource
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        if (($model = NewsResource::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException("Object not found: $id");
    }
}