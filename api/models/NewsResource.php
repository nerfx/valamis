<?php
/**
 * Created by PhpStorm.
 * User: Nerf
 */

namespace api\models;

use common\models\News;
use yii\db\ActiveQuery;
use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;

/**
 * Class NewsResource
 * @package api\models
 *
 * @property NewsCommentResource[] $commentResources
 */
class NewsResource extends News implements Linkable
{
    /**
     * @return ActiveQuery
     */
    public function getCommentResources()
    {
        return $this->hasMany(NewsCommentResource::class, ['news_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function extraFields()
    {
        return [
            'comments' => function (self $model) {
                return $model->commentResources;
            },
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['news/view', 'id' => $this->id], true),
            'edit' => Url::to(['news/view', 'id' => $this->id], true),
            'comments' => Url::to(['news/comments', 'id' => $this->id], true),
            'comment' => Url::to(['news/comments', 'id' => $this->id], true),
            'index' => Url::to(['news/index'], true),
        ];
    }
}