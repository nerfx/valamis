<?php
/**
 * Created by PhpStorm.
 * User: Nerf
 */

namespace api\models;


use common\models\NewsComment;

class NewsCommentResource extends NewsComment
{
    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        return ['id', 'text'];
    }
}