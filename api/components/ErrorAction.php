<?php
/**
 * Created by PhpStorm.
 * User: Nerf
 */

namespace api\components;

use Yii;
use yii\web\ErrorAction as BaseErrorAction;
use yii\web\HttpException;

class ErrorAction extends BaseErrorAction
{
    public function run()
    {
        Yii::$app->getResponse()->setStatusCodeByException($this->exception);

        if ($this->exception instanceof HttpException) {
            $code = 0;
            $status = $this->exception->statusCode;
        } else {
            $code = $this->exception->getCode();
            $status = 500;
        }

        $data = [
            'name' => $this->getExceptionName(),
            'message' => $this->getExceptionMessage(),
            'code' => $code,
            'status' => $status,
            'type' => get_class($this->exception),
            'exception' => $this->exception,
        ];
        if (!YII_DEBUG) {
            unset($data['type']);
            unset($data['exception']);
        }

        Yii::$app->getResponse()->data = $data;

        return Yii::$app->getResponse();
    }
}